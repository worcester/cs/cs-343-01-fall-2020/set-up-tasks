*Version 2020-Fall-1.0, Revised 1 September 2020*
### *CS-343 01 &mdash; Fall 2020*

# Set-up Tasks

## Objectives
The objective of this assignment is for students to get themselves set up with the tools and websites that they will need for assignments during the semester.

Each of the five tasks below is worth 1 Token. Completing all of them by their due dates will let you start the semester with 5 Tokens.

***Note: All of these tasks will have to be done at some point in the semester anyway to be able to complete a particular assignment in the course. So you might as well do it now, and earn a Token.***

## Task 1: Join Discord
### *Worth 1 Token*

### Introduction
We will be using Discord as a communication tool within the class as a whole, and within your teams later in the semester.

Follow the [invitation link](https://discord.gg/CkbZWpX) to join the WSU CS-343 Fall 2020 server, and change your nickname to your first and last name as on the class list.

### Deliverable Specification
Your assignment must meet *all* of the following specifications by **2 September 2020 at 11:00am** to earn 1 token.

* Join the Discord Server. 
* Change your nickname to include both your first and last name.

## Task 2: GitLab Account
### *Worth 1 Token*

### Introduction
We will be using GitLab.com for storing code and sharing it with teammates.

As GitLab.com is a cloud service outside of WSU, you must create your own account, and inform me of the username so that I can add you to the team for the course.

### Create a GitLab.com Account
If you do not already have an account on GitLab.com, go to [https://gitlab.com/users/sign_in](), select the *Register* tab, and create an account.

### Deliverable Specification
Your assignment must meet *all* of the following specifications by the **2 September 2020 at 11:00am** to earn 1 token.

* Fill in this [form](https://forms.gle/jCDDkn3g1JN862Wb8).
* You must be logged into your WSU email account.

## Task 3: Blog Set-up
### *Worth 1 Token*

### Introduction

Two of the CS Program-Level Student Learning Outcomes that this course addresses are:

> * Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Mastery)
> * Communicate effectively both in written and oral form. (Mastery)

You will be required to read outside blogs, articles, and/or books; and/or listen to podcasts on your own and keep your own blog about those that you found useful/interesting. Your blog must be publicly accessible[^1], and will be aggregated on the [CS@Worcester Blog](http://cs.worcester.edu/blog/).

[^1]: If there is a reason why your name cannot be publicly associated with this course, you may blog under a pseudonym. You must see me to discuss the details, but your blog must still be publicly accessible and aggregated, and you must inform me of your pseudonym.

### Create Blog

If you do not already have your own blog, create one. There are many places that will let you create a free blog. If you have no preference, I suggest using [WordPress](http://wordpress.com).

Some notes:

1. **Naming your blog**. I suggest that you do not use a name that includes the title or number of this course, or that specifically references Worcester State. This should be a professional blog that you can post to throughout your career. Blogging is the way that many computing professionals record what they have done for their future use, to help others learn how to use new tools and technologies, and act as a professional portfolio. 
2. **Accessibility of your blog.** You need to make your blog publicly accessible. You may blog under a pseudonym if you wish, but you must let me know the name of your blog.
3. **Tagging or Categories.** When you write blog posts, you should tag them based on the topic, or you should create categories to post them under, or both. The difference between tags and categories is unclear, and differs from blogging platform to platform. The reason for tagging your posts is not just to organize them for yourself and others, but also to allow you to syndicate your posts to different sites  and for different courses, based on content. 
    * You must use the tag `CS@Worcester` on your posts for this course, so that they can be aggregated to the CS@Worcester blog. (You can get a feel for how this is done by looking at [my blog](http://blog.karl.w-sts.com/).
    * You  must use the tag `CS-343` on your posts for this course.

### Write Introductory Blog Post

Write a blog post introducing your blog for this course and tag it with `CS@Worcester` and `CS-343`.

### Link to CS Blog

Find the RSS or Atom feed for the `CS@Worcester` tag and post it as a comment on the [How To Contribute To This Blog page on the CS@Worcester blog](http://cs.worcester.edu/blog/how-to-contribute/). There are some instructions there for figuring out the feed URI.

### Deliverable Specification
Your assignment must meet *all* of the following specifications by **8 September at 11:59pm** to earn 1 token:

* A blog post introducing your blog for this course.
* The tag `CS@Worcester` on that post.
* The tag `CS-343` on that post.
* The RSS or Atom feed URI for your `CS@Worcester` tag posted as a comment on [How To Contribute To This Blog page on the CS@Worcester blog](http://cs.worcester.edu/blog/how-to-contribute/).

## Task 4: Blog Discovery
### *Worth 1 Token*

### Introduction
You will be reading blogs related to the content of this course.

To get you started finding these blogs, you must find one blog related to the content of the course and share it with the class.

### Find a Blog Related to the Course Material
Find a blog that posts material related to the content of this course. Look at the syllabus to see what sorts of topics are appropriate.

### Deliverable Specification
Your assignment must meet *all* of the following specifications by **8 September at 11:59pm** to earn 1 token.

* Post to the CS-343 Discord server in the `#general` channel.
* Your post must include the URL for the blog.
* Your post must include a one-sentence description of the blog.
* Your post must include a single sentence about why you chose this blog.

## Task 5: Podcast Discovery
### *Worth 1 Token*

### Introduction
You will be listening to podcasts related to the content of this course.

To get you started finding these podcasts, you must find one podcast related to the content of the course and share it with the class.

### Find a Podcast Related to the Course Material
Find a podcast that discusses material related to the content of this course. Look at the syllabus to see what sorts of topics are appropriate.

### Deliverable Specification
Your assignment must meet *all* of the following specifications by **8 September at 11:59pm** to earn 1 token.

* Post to the CS-343 Discord Server in the `#general` channel.
* Your post must include the URL for the podcast.
* Your post must include a one-sentence description of the podcast.
* Your post must include a single sentence about why you chose this podcast.

#### &copy; 2020 Karl R. Wurst, kwurst@worcester.edu

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.